module Graphics.Rendering.HelpGL (emptyRGBATexture) where

import Foreign.Marshal.Array
import Foreign.Ptr
import Graphics.Rendering.OpenGL as GL

(?) :: (Integral a, Num b) => () -> a -> b
(?) _ = fromIntegral

emptyRGBATexture :: Int -> Int -> IO ()
emptyRGBATexture w h =
  texImage2D Texture2D NoProxy 0 RGBA' (TextureSize2D (() ?w) (() ?h)) 0 (PixelData RGBA UnsignedByte nullPtr)
